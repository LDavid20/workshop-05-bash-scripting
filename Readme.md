
# Bash scripting

Para este sección se busca crear muchas automatización de procesos

Inicialmente arrancamos `vagrant` y una vez dentro entramos al archivo `bashrc`

![coamdo tree](img/1.jpg "Archivo de Imagen")

El archivo `bashrc` se ejecutar al iniciar vagrant, comprendiendo esto incresamos al archivo y creamos un mensaje de `hola`

![coamdo tree](img/2.jpg "Archivo de Imagen")

Guardamos y por medio el comando `source` mas el nombre del archivo a poder ver el mensaje
![coamdo tree](img/3.jpg "Archivo de Imagen")

Muestra del mensaje
![coamdo tree](img/4.jpg "Archivo de Imagen")

Ahora bien, vamos a crear un usuario nuevo en vagrant por medio del comando `sudo adduser nombreusuario`, en este caso mi usuario se llamara `ldavid` y lo llenamos con los datos pertinentes, cabe recalcar que la clave es `secret` por fines practicos
![coamdo tree](img/5.jpg "Archivo de Imagen")

Luego inigresamos a la cuenta del usuario, por medio de `su`
![coamdo tree](img/6.jpg "Archivo de Imagen")

Luego si quisieramos realizar un mensaje dirente al iniciar, el usuario vamos igual al archivo de `.bashrc`, pero como el usuario nuevo no tiene el `vim` enchulado, vamos a utilizar el repositorio del renombrado `Misael Matamoros`
![coamdo tree](img/7.jpg "Archivo de Imagen")

Dentro de hay seguimos las instrucciones al pie de la letras
![coamdo tree](img/8.jpg "Archivo de Imagen")

Luego al finalizar entramos al archivo `.bashrc` para hacer los cambios
![coamdo tree](img/9.jpg "Archivo de Imagen")

Dentro de este archivo, activamos el alias de la siguiente forma
![coamdo tree](img/10.jpg "Archivo de Imagen")

Antes de seguir editando el archivo vamos a `https://www.coolgenerator.com/ascii-text-generator`, donde creamos un diseño con el codigo ascii
![coamdo tree](img/13.jpg "Archivo de Imagen")

Esto lo agregamos al final del archivo 
![coamdo tree](img/14.jpg "Archivo de Imagen")

Luego lo ejecutamos y veremos lo genial que se ve
![coamdo tree](img/15.jpg "Archivo de Imagen")

Comprobado lo anterior, creamos una nueva key como el siguiente nombre, esto lo hacemos en otra consola
![coamdo tree](img/16.jpg "Archivo de Imagen")

Luego nuevamos en la seccion de `ldavid`, creamos un directorio `.ssh`, el cual tendremos que cambiar luego los permisos 
![coamdo tree](img/17.jpg "Archivo de Imagen")

Dentro de esa ubicación, creamos el archivo de `authorized_keys` y lo editamos
![coamdo tree](img/18.jpg "Archivo de Imagen")

En el pegaremos lo que tiene la llave publica de `isw811-320-g2.pub`, asi como se muestra
![coamdo tree](img/19.jpg "Archivo de Imagen")

Volvemos al usuario de `vagrant` y entramos al siguiente archivo

![coamdo tree](img/20.jpg "Archivo de Imagen")

En el creamos un mensaje

![coamdo tree](img/21.jpg "Archivo de Imagen")

Luego cambiamos los permisos para que se ejecute el archivo

![coamdo tree](img/22.jpg "Archivo de Imagen")

Lo probamos de la siguiente forma

![coamdo tree](img/23.jpg "Archivo de Imagen")

Comprobado esto vamos a crear un directorio y movemos el archivo de saludar

![coamdo tree](img/24.jpg "Archivo de Imagen")

Volvemos a cambiarle los permisos para solo ser usados por `root` 

![coamdo tree](img/25.jpg "Archivo de Imagen")

Entramos al usuario creado, y podremos de igual forma ejecutar el saludo

![coamdo tree](img/26.jpg "Archivo de Imagen")

Ahora bien proseguimos con los siguientes comandos

![coamdo tree](img/27.jpg "Archivo de Imagen")

a

![coamdo tree](img/28.jpg "Archivo de Imagen")

Los siguientes comando es para crear una enlase a la ruta, de una manera mas rapida

![coamdo tree](img/29.jpg "Archivo de Imagen")

Para ello, creamos el siguiente directorio llamado `scripts`, esto en la siguiente direccion

![coamdo tree](img/30.jpg "Archivo de Imagen")

Volvemos al directorio raiz y creamos el archivo de `conectar` que luego lo abrimos por medio de code

![coamdo tree](img/31.jpg "Archivo de Imagen")

Dentro de el vamos a crear lo siguiente para crear la conexion de una forma rapida, con solo ejecutar el archivo, facil y rapido

![coamdo tree](img/32.jpg "Archivo de Imagen")


Ahora para proseguir vamos a utilizar los archivos proporcionados por el profe

![coamdo tree](img/33.jpg "Archivo de Imagen")

Lo descargamos y lo cocamos en la carperta `scripts`

![coamdo tree](img/34.jpg "Archivo de Imagen")

Luego verificamos si estos esta
![coamdo tree](img/35.jpg "Archivo de Imagen")

Creamos una copia que estara en el usuario de `vagrant` y de igual forma lo chequeamos

![coamdo tree](img/36.jpg "Archivo de Imagen")

Ya que teniamos el mysql instalado, entramos a el por medio del comando `sudo mysql`

![coamdo tree](img/37.jpg "Archivo de Imagen")

Creamos la siguiente base de datos, el usuario `laravel` y cambiamos los privilegios. Y refrescamos estos privilegio

![coamdo tree](img/38.jpg "Archivo de Imagen")

Salimos de la base de datos y entramos nuevamente a la base de datos por el usuario creado, y verificamos la base de datos

![coamdo tree](img/39.jpg "Archivo de Imagen")

Luego importamos los dats del archivo copiado a la base de datos

![coamdo tree](img/40.jpg "Archivo de Imagen")

Si le damos `show tables` veremos que se cargo perfectamente

![coamdo tree](img/41.jpg "Archivo de Imagen")

Luego importamos los datos a la base de datos

![coamdo tree](img/42.jpg "Archivo de Imagen")

Ya con esto podremos ver que tenemos todos los datos perfectamente

![coamdo tree](img/43.jpg "Archivo de Imagen")

Luego de esto volvemos a la consola principal y creamos un archivo de `backup.sh`

![coamdo tree](img/44.jpg "Archivo de Imagen")

En este archivo vamos a crear el siguiente codigo, para crear un backup de la base de datos

![coamdo tree](img/45.jpg "Archivo de Imagen")

Luego creamos los siguientes directorios

![coamdo tree](img/46.jpg "Archivo de Imagen")

Cambiamos los permisos del archivo

![coamdo tree](img/47.jpg "Archivo de Imagen")

Luego ejecutamos el archivo y comprobamos que se haga la copia

![coamdo tree](img/48.jpg "Archivo de Imagen")

Creamos el archivo de `crontab`, para programar la creacion de backups en un periodo determinado

![coamdo tree](img/50.jpg "Archivo de Imagen")

Al final creamos el siguiente codigo, para programar estos backup de la base de datos

![coamdo tree](img/49.jpg "Archivo de Imagen")

Y bueno por utilimos nos enseño el profesor a volvar la base de datos en casos muy explicitos.
![coamdo tree](img/51.jpg "Archivo de Imagen")

Con esto terminarimos la clase de hoy



